﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CefWebkit.Binding
{
    public class ConfigurationMode : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 学习地址
        /// </summary>
        private string studyAddress;

        /// <summary>
        /// 设置或获取学习地址
        /// </summary>
        public string StudyAddress
        {
            get { return studyAddress; }
            set
            {
                studyAddress = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("StudyAddress"));
                }
            }
        }

        /// <summary>
        /// 学习开始时间
        /// </summary>
        private DateTime studyTimeStart;

        /// <summary>
        /// 设置或获取学习开始时间
        /// </summary>
        public DateTime StudyTimeStart
        {
            get { return studyTimeStart; }
            set
            {
                studyTimeStart = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("StudyTimeStart"));
                }
            }
        }

        /// <summary>
        /// 学习结束时间
        /// </summary>
        private DateTime studyTimeEnd;

        /// <summary>
        /// 设置或获取学习开始时间
        /// </summary>
        public DateTime StudyTimeEnd
        {
            get { return studyTimeEnd; }
            set
            {
                studyTimeEnd = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("StudyTimeEnd"));
                }
            }
        }

        /// <summary>
        /// Windows服务配置文件路径
        /// </summary>
        private string winServerCfgPath;

        /// <summary>
        /// 设置或获取Windows服务配置文件路径
        /// </summary>
        public string WinServerCfgPath
        {
            get { return winServerCfgPath; }
            set
            {
                winServerCfgPath = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("WinServerCfgPath"));
                }
            }
        }

    }
}
