﻿using CefWebkit.Binding;
using CefWebkit.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CefWebkit
{
    /// <summary>
    /// InputPassword.xaml 的交互逻辑
    /// </summary>
    public partial class Configuration : Window
    {

        private string strCurrentAppDir = FilePathHelper.GetCurrentAppRootPath();//当前应用的根路径
        private string strWinServerCfgPath = string.Empty;//当前应用的根路径
        public Configuration()
        {
            InitializeComponent();
            InitializePanelData();
            this.Topmost = true;
        }

        /// <summary>
        /// 初始化面板数据
        /// </summary>
        private void InitializePanelData()
        {

            ConfigurationMode model = new ConfigurationMode();
            DateTime dateNow = DateTime.Now;
            strWinServerCfgPath = FileHelper.GetIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "WinServerCfgPath", "Value");
            string strStartTime = FileHelper.GetIniData(strWinServerCfgPath, "Times", "Start");
            string strEndTime = FileHelper.GetIniData(strWinServerCfgPath, "Times", "End");
            try
            {
                model.StudyTimeStart = Convert.ToDateTime(dateNow.ToString("yyyy-MM-dd " + strStartTime + ":00"));
                model.StudyTimeEnd = Convert.ToDateTime(dateNow.ToString("yyyy-MM-dd " + strEndTime + ":00"));
            }
            catch
            {
                model.StudyTimeStart = Convert.ToDateTime(dateNow.ToString("yyyy-MM-dd 00:00:00"));
                model.StudyTimeEnd = Convert.ToDateTime(dateNow.ToString("yyyy-MM-dd 01:00:00"));
                //MessageBox.Show(this,"请检查配置文件【" + strWinServerCfgPath + @"】中的Times\Start或Times\End配置节！",
                //    "提示",
                //    MessageBoxButton.OK,
                //    MessageBoxImage.Asterisk);
                //this.Close();
                //return;
            }
            model.StudyAddress = FileHelper.GetIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "URL", "Value");
            model.WinServerCfgPath = strWinServerCfgPath;
            this.DataContext = model;
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ConfigurationMode model = this.DataContext as ConfigurationMode;
                //正则判断
                string Pattern = @"^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&$%\$#\=~])*$";
                Regex r = new Regex(Pattern);
                Match m = r.Match(model.StudyAddress);
                //if (!m.Success)
                //{
                //    MessageBox.Show(this,"请检查输入的学习地址是否为域名！",
                //        "提示",
                //        MessageBoxButton.OK,
                //        MessageBoxImage.Asterisk);
                //    return;
                //}
                if (string.IsNullOrEmpty(model.WinServerCfgPath))
                {
                    MessageBox.Show(this,"请输入Windows服务配置文件的路径！",
                        "提示",
                        MessageBoxButton.OK,
                        MessageBoxImage.Asterisk);
                    return;
                }
                else
                {
                    if (!File.Exists(model.WinServerCfgPath))
                    {
                        MessageBox.Show(this,"Windows服务配置文件的路径不存在【"+ model.WinServerCfgPath + "】！",
                        "提示",
                        MessageBoxButton.OK,
                        MessageBoxImage.Asterisk);
                        return;
                    }
                }
                bool bSaveStatus = true;
                if (!FileHelper.WriteIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "URL", "Value", model.StudyAddress))
                {
                    MessageBox.Show(this,"保存学习地址【" + model.StudyAddress + "】失败！",
                        "失败",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    bSaveStatus = false;
                }
                if (!FileHelper.WriteIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "WinServerCfgPath", "Value", model.WinServerCfgPath))
                {
                    MessageBox.Show(this,"保存Windows服务配置文件路径【" + model.WinServerCfgPath + "】失败！",
                        "失败",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    bSaveStatus = false;
                }
                if (!FileHelper.WriteIniData(model.WinServerCfgPath, "Times", "Start", model.StudyTimeStart.ToString("HH:mm")))
                {
                    MessageBox.Show(this,"保存学习开始时间【" + model.StudyTimeStart.ToString("HH:mm") + "】失败！",
                        "失败",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    bSaveStatus = false;
                }
                if (!FileHelper.WriteIniData(model.WinServerCfgPath, "Times", "End", model.StudyTimeEnd.ToString("HH:mm")))
                {
                    MessageBox.Show(this,"保存学习结束时间【" + model.StudyTimeEnd.ToString("HH:mm") + "】失败！",
                        "失败",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    bSaveStatus = false;
                }
                if (bSaveStatus)
                {
                    MessageBox.Show(this,"操作成功！",
                        "提示",
                        MessageBoxButton.OK,
                        MessageBoxImage.Asterisk);
                }
            }
            catch
            {
                MessageBox.Show(this,"操作失败！",
                        "失败",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                return;
            }

            
            //this.Close();            
        }
        
    }
}
