﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// 密码类型的枚举
/// </summary>
public enum PassTypeEnum
{
    /// <summary>
    /// 解锁
    /// </summary>
    UnLock = 0,

    /// <summary>
    /// 管理密码
    /// </summary>
    Admin = 1,
}
