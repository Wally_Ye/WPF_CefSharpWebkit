﻿using CefSharp;
using CefWebkit.CefSharpLib;
using CefWebkit.Lib;
using CefWebkit.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CefWebkit
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool bDebug = false;//是否为开发模式
        private string strCurrentAppDir = FilePathHelper.GetCurrentAppRootPath();//当前应用的根路径
        private string strUrl = "http://lfclj.com";//默认跳转的URL
        public MainWindow()
        {
            GetConfigValues();

            double dWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double dHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            this.WindowState = WindowState.Maximized;
            this.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            this.Width = dWidth;
            this.Height = dHeight;
            InitializeComponent();

            if (!bDebug)
            {
                //无边框
                this.WindowStyle = WindowStyle.None;
                //窗体置顶
                this.Topmost = true;
            }
            else
            {
                //屏蔽Alt+F4
                this.PreviewKeyDown -= Window_PreviewKeyDown;
                this.PreviewKeyUp -= Window_PreviewKeyUp;
                toolBar.Height = double.NaN;
            }



            //在本窗体中打开连接
            Browser.LifeSpanHandler = new OpenPageSelf();
            //下载接口实现
            Browser.DownloadHandler = new DownloadHandler();
            Browser.Address = strUrl;
        }

        /// <summary>
        /// 获取配置的值
        /// </summary>
        private void GetConfigValues()
        {
            try
            {
                //FileHelper.WriteIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "URL", "Value", @"http://member.chinaacc.com/member/loginAgain.shtm?validateLogin=n&gotoURL=http://member.chinaacc.com/home/index.shtml#/index/stu");
                bDebug = Convert.ToBoolean(FileHelper.GetIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "Debug", "Value"));
                string url = FileHelper.GetIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "URL", "Value");
                if (!string.IsNullOrEmpty(url))
                {
                    strUrl = url;
                }
            }
            catch
            {

            }
        }

        private void miBrowserBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Browser.GetBrowser().GoBack();
            }
            catch
            {

            }
        }

        private void miBrowserForward_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Browser.GetBrowser().GoForward();
            }
            catch
            {

            }
        }

        private void miBrowserRefresh_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Browser.GetBrowser().Reload();
            }
            catch
            {

            }
        }

        private void miConfig_Click(object sender, RoutedEventArgs e)
        {
            //Configuration configWindow = new Configuration();
            //configWindow.ShowDialog();
            //return;

            InputPassword passWindow = new InputPassword(PassTypeEnum.Admin);
            passWindow.ShowDialog();
            if (passWindow.IsPass)
            {
                //打开配置文件窗口
                Configuration configWindow = new Configuration();
                configWindow.ShowDialog();
            }

        }

        private void miQuit_Click(object sender, RoutedEventArgs e)
        {
            if (closeWindow())
            {
                this.Close();
            }
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!closeWindow())
            {
                e.Cancel = true;
            }
        }

        private bool closeWindow()
        {
            bool bRes = false;
            string strWinServerCfgPath = FileHelper.GetIniData(strCurrentAppDir + "\\Config\\CefWebkit.ini", "WinServerCfgPath", "Value");
            if (string.IsNullOrEmpty(strWinServerCfgPath) || File.Exists(strWinServerCfgPath) == false)
            {
                MessageBox.Show("请检查配置文件【" + strCurrentAppDir + @"\Config\CefWebkit.ini" + @"】中的WinServerCfgPath\Value配置参数！",
                                "错误",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                FileHelper.WriteLog("请检查配置文件【" + strCurrentAppDir + @"\Config\CefWebkit.ini" + @"】中的WinServerCfgPath\Value配置参数！");
                return bRes;
            }
            //判断是否到达设定的学习时间
            try
            {
                string strStartTime = FileHelper.GetIniData(strWinServerCfgPath, "Times", "Start");
                string strEndTime = FileHelper.GetIniData(strWinServerCfgPath, "Times", "End");
                DateTime dateNow = DateTime.Now;
                try
                {
                    DateTime dateStart = Convert.ToDateTime(dateNow.ToString("yyyy-MM-dd " + strStartTime + ":00"));
                    DateTime dateEnd = Convert.ToDateTime(dateNow.ToString("yyyy-MM-dd " + strEndTime + ":00"));
                    if (dateEnd < dateStart)
                    {
                        dateEnd = dateEnd.AddDays(+1);
                    }

                    if (dateNow < dateStart || dateNow > dateEnd)
                    {
                        bRes = true;
                    }
                }
                catch
                {
                    FileHelper.WriteLog("请检查配置文件【" + strCurrentAppDir + @"\Config\ServiceConfig.ini" + @"】中的Times\Start或Times\End配置节！");
                    //return;
                }

                InputPassword passWindow = new InputPassword();
                passWindow.ShowDialog();
                if (passWindow.IsPass)
                {
                    //正在学习时间设置Windows服务配置的参数
                    FileHelper.WriteIniData(strWinServerCfgPath, "Closed", "Value", "true");//强制关闭标识
                    FileHelper.WriteIniData(strWinServerCfgPath, "Closed", "Time", dateNow.ToString("yyyy-MM-dd"));//强制关闭标识时间
                    bRes = true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(this,"程序出现未知的错误！" + ex.ToString(),
                "错误",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
                bRes = false;
            }
            return bRes;
        } 

        private bool AltDown = false;
        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.SystemKey == Key.LeftAlt || e.SystemKey == Key.RightAlt)
            {
                AltDown = true;
            }
            else if ( (e.SystemKey == Key.F4 && AltDown))
            {
                e.Handled = true;
            }
            else if(e.Key == Key.Escape)
            {
                if(toolBar.Height == 0)
                {
                    toolBar.Height = double.NaN;
                }
                else
                {
                    toolBar.Height = 0;
                }
            }
        }

        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.SystemKey == Key.LeftAlt || e.SystemKey == Key.RightAlt)
            {
                AltDown = false;
            }
            
        }

    }
}
