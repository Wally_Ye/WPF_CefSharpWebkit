# WPF_CefSharpWebkit

#### 项目介绍
使用VS2017 .Net 4.5.2 构建的 WPF/CefSharp63； 支持mp3、mp4、flash播放；支持AnyCpu模式编译。(其实就是一个用WPF开发的，带壳的webkit浏览器)

#### 文件目录说明
packages/ 使用 NuGet获取 CefSharp63后的文件路径；

packages/cef.redist.x64.3.3239.1723/ 为支持mp3、mp4格式，替换了部分dll

packages/cef.redist.x86.3.3239.1723/ 为支持mp3、mp4格式，替换了部分dll

CefWebkit/ 为项目主文件夹

CefWebkit/Cache 为开启CefSharp的Cookie支持，使用的存储目录

CefWebkit/CefSharpLib CefSharp的一些接口实现。

CefWebkit/Config 项目的配置文件，如：首页的url地址等配置存储在此，

CefWebkit/Lib 项目的一些扩展类

CefWebkit/Plugins CefSharp支持flansh播放使用的插件

CefWebkit/Resource 项目资源文件夹

#### html5test.com 测试 测试时间【2018-05-29】 测试地址【http://html5test.com/】
![输入图片说明](https://gitee.com/uploads/images/2018/0529/161004_8ce292e1_688980.png "H5支持测试结果.png")
与Chrome66.0.3359.181 on Windows 10 相比：
![输入图片说明](https://gitee.com/uploads/images/2018/0529/161110_2b8a6db7_688980.png "Chrome63 H5支持测试结果.png")


